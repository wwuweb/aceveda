<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>March 2015 Blog | Marine Mammal Ecology Lab | Western Washington University</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="https://www.wwu.edu/wwucommon/lite/css/main.css">
  <link rel="stylesheet" type="text/css" href="/faculty/aceveda/customize/css/customize.css">

  <link rel="shortcut icon" href="https://www.wwu.edu/wwucommon/favicon.ico" type="image/vnd.microsoft.icon">
  <!--#include virtual="/faculty/aceveda/customize/analytics.html"-->
</head>

<body>
  <!--#include virtual="/faculty/aceveda/includes/skip-links.shtml"-->
  <div class="zen-wrapper">
    <!--#include virtual="/faculty/aceveda/customize/site-name.html"-->
    <!--#include virtual="/faculty/aceveda/includes/header.html"-->

    <!--#include virtual="/faculty/aceveda/customize/nav.html"-->

    <div class="body-wrapper">
      <main id="main-content">
        <div id="content-wrapper">
          <!-- Place content here -->
          <div id="content-nosidebar"><!-- InstanceBeginEditable name="Content" -->
            <h1>March 2015</h1>
            <h2>A short history of why I'm here - marine mammal bioacoustics</h2>
            <h3>Kat Nikolich, M.Sc. student</h3>
            <h4>8 March 2015</h4>
            <p>When I was a child, I thought the ocean must be a frightfully silent place.</p>

            <p>On vacation in Hawaii with my family, I would slip beneath the waves and experience total sensory deprivation. The cacophony of people, dogs and music from the beach faded away, and a pressurized silence enveloped me for that few seconds before I broke the surface again. My ears, poorly equipped for hearing underwater, told me that the ocean was a realm devoid of sound. That is, until the time I heard the song of a humpback whale.</p>

            <p>The sound seemed to come from everywhere at once, so deep and loud that I could feel it reverberating through my chest, overlaid with a long moan that sounded like it was coming from only meters away. I looked everywhere, but couldn't find the source of that sound. I know now that the male humpback whale producing that song could have been kilometers away, and that sound travels so fast and far underwater that it was delivered to even my maladapted terrestrial ears mostly unchanged over such a distance.</p>

            <p>That whale song taught me that the ocean is not silent. Decades before I was born, that same haunting song helped spark a revolution. In 1967, Roger Payne and Scott McVay told the world that humpback whales sing underwater in their Hawaiian breeding grounds. This opened the door to the possibility that whales and their relatives may lead complex social lives, and possess intelligence that doesn't translate well through the air-water interface. This discovery broke the silence in many ways, leading to the formation of the Save the Whales movement and a generation of people that saw whales not as a commodity, but as fellow sentient creatures.</p>

            <p>Since then, the study of marine mammal acoustics has opened a number of doors into the lives of whales, dolphins, and other marine mammals. As animals that spend the majority of their lives underwater and travel vast expanses of open ocean, marine mammals are good at keeping their private lives private. The most reliable instruments we have for observing behaviour -our eyes- are all but useless when it comes to understanding what whales are doing, where they go and how they interact with each other. However, marine mammals all use sound at some point in their lives to perform important tasks like finding food, attracting mates, navigating and recognizing one another. Realizing this, marine mammal biologists have started using our ears, rather than our eyes, to observe these animals. And once we started to listen, it's startling how much they have to say.</p>

            <p>My interest in marine mammal acoustics started while I was working as a naturalist on a whale watching boat out of Victoria, BC. It was my summer job while I was completing my Bachelor's degree at the University of Victoria, and spending so much time around the local killer whale pods brought back my childhood love of this species (I have watched Free Willy approximately 200 times) and bridged it with my current interest in marine conservation. On the whale watching boat, we had a small hydrophone that we would dip over the side so that our customers could hear the sounds of the animals below us. Hearing the incredibly complex and diverse vocalizations led me to ponder the age-old question: what are they saying? I read every paper I could find on killer whale communication, and the more I learned, the more I realized what scientists were just beginning to uncover: that sound was just another way in which humanity was polluting the ocean, making it harder and harder for marine mammals to perform those all-important vocal tasks. The field of acoustics was merging with the field of conservation, and I wanted in.</p>

            <p>After I graduated with a B.Sc. in Biology, I resolved to get more experience with marine mammal acoustics. That summer, I took an internship with the Alaska Whale Foundation studying humpback whale calls in Frederick Sound. I lived in a remote lighthouse for a month and immersed myself in the social sounds of the whales that use the area as their feeding grounds. The researcher I worked for, Michelle (pictured with me (left) in the photo with the lighthouse), was a master's student at Oregon State University. Her enthusiasm for acoustics further inspired me to apply to graduate school. Soon after my experience in Alaska, I took another internship working with a researcher from Murdoch University in Australia who was studying the relationship of spinner dolphins and tourist boats near Kona, Hawaii. By the time I got back to Victoria, I had applied and been accepted to my lab here at Western Washington University. During the summer between, I sought out more experience with acoustics and found myself working with Jared Towers, the director of the Marine Education and Research Society, on a species that has always fascinated me: the minke whale. I had the unenviable job of painstakingly poring through hundreds of hours of underwater recordings from the north coast of Vancouver Island, searching for the shy calls of the minke. I loved every minute of the tedious work, and I knew that I was ready to embark on a research project of my own. </p>
            <p>
              <p class="center"><img src="../images/photos/for%20blogs/KN_Blog_08032015_1.jpg" alt="Kat and a friend smiling on a boat"></p>

              <p>Here at Western, I've had the opportunity to branch out and study the vocalizations of another group of marine mammals: the pinnipeds. While whales and dolphins may be the more famous vocalists, seals, sea lions and walruses also use underwater sound for a number of important functions. I am focusing on the most abundant marine mammal in our coastal waters, the harbour seal (<EM>Phoca vitulina</EM>). Very little is known about the vocalizations of seals in this part of the world, and whether human noise can affect them the way it does with killer whales and other species. This is what I hope to learn more about.</p>

              <p>Male harbour seals use calls to advertise themselves during the breeding season, which takes place during the summer. Studies done on harbour seals in other parts of the world have shown that these calls are slightly different depending on where the seal comes from. Just as people from different parts of the USA speak differently depending on where they grew up, so too do seals have different dialects that allow us to identify where they come from. Nobody knows what 'accent' harbour seals in the Pacific Northwest have, because nobody's really bothered to listen. The primary objective of my study is to characterize their calls, including the size of their repertoire (the number of unique calls they make, like words in a vocabulary) and when they call the most. Are they chattiest at day or during the night? At high tide or low tide? In July or August? Because their ability to breed successfully may depend on their ability to hear one another's calls, I'm also interested in whether human noise has an effect on their vocal activity.</p>

              <p>To this end, I spent last summer on beautiful Hornby Island, BC (see map and photo of my field site). Joined by my eight indomitable field assistants (stay tuned for a full account of my field work in my next blog post), I collected over 300 hours of observations and 2100 hours of acoustic recordings. I'm currently going through these mounds of data to piece together what the seals at Hornby Island are saying, and how they deal with the intense amount of background noise that humanity has added to their environment in recent decades. </p>
              <p>
                <p class="center"><img src="../images/photos/for%20blogs/KN_Blog_08032015_2.jpg" alt="satellite map of Puget Sound"></p>

                <p>I no longer associate the ocean with suffocating silence. Blessed with the technology that allows me to hear underwater, I've learned that the ocean is never silent, and that even the smallest of noises can give you a wealth of information. Even in the absence of the din produced by boats and other human machinery, the marine environment is a constant conversation of waves breaking on shore, snapping shrimp crackling like bacon in a skillet, rockfish grunting like old men as they traverse the gravelly bottom, and the occasional and unmistakable syllables of whale song. The ocean is a noisy place, but if you listen correctly, it can tell you a story. As an acoustician, my goal is to interpret those stories and share them with the world. </p>
                <p>
                  <p class="center"><img src="../images/photos/for%20blogs/KN_Blog_08032015_3.jpg" alt="rocky beach with seals laying about"></p>

                  <p>Give me your best, harbour seals. I'm all ears. </p>

                  <!-- InstanceEndEditable --></div>

                </div> <!-- End content-wrapper -->
              </main> <!-- End main-content -->
            </div> <!-- End body-wrapper -->


            <!--#include virtual="/faculty/aceveda/customize/footer.shtml"-->
            <!--#include virtual="/faculty/aceveda/includes/footer-sub.shtml"-->
            <!--#include virtual="/faculty/aceveda/includes/scripts.html"-->
            <!-- Put Scripts here -->
          </div>
        </body>
        </html>

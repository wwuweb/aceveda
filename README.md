# aceveda
This repo exists for the sole purpose of expediting the conversion of the faculty.wwu.edu/aceveda website to lite responsive template.  
When that project is done this repo should be deleted.

## General process:
1.  Procure a STS-Aceveda ticket from JIRA. (If there are none then the project is done and you can delete this repo).
2.  That ticket will list 10 html files that need re-working.  The file "index.shtml" should be used as the baseline.
3.  Move the original file to filename.html.bck where filename is the name of the file that you're currently working on.
4.  Copy the html file index.shtml to filename.shtml, again, where filename is the name of the file that you're currently working on.
5.  In filename.shtml delete the content between the <div id="content-wrapper"> tag and it's ending </div> tag.
6.  Replace the deleted html with the corresponding html between the same 2 tags in the filename.html.bck.  
7.  Add styling if needed.  Don't add the styling to customize.css.  To avoid stepping on each other's changes and initiating git merge hell, I'd add the styling changes to 
    a customize/filename.css file.  
    For instance, let's say you added styling for the CurrentUG.shtml page to a file called /customize/CurrentUG.css.  For these css changes to take effect, you need to:
    - Edit the /faculty/aceveda/People HTML files/CurrentUG.shtml file, add the following line just before the ending </head>.  
      <link rel="stylesheet" type="text/css" href="/faculty/aceveda/customize/CurrentUG.css">
8.  Delete the filename.html.bck file.
9.  That's all for now.  Feel free to contact Alex Waltrip with any questions.

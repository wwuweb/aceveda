<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Home | Marine Mammal Ecology Lab | Western Washington University</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="https://www.wwu.edu/wwucommon/lite/css/main.css">
  <link rel="stylesheet" type="text/css" href="/faculty/aceveda/customize/css/customize.css">

  <link rel="shortcut icon" href="https://www.wwu.edu/wwucommon/favicon.ico" type="image/vnd.microsoft.icon">
  <!--#include virtual="/faculty/aceveda/customize/analytics.html"-->
</head>

<body>
  <!--#include virtual="/faculty/aceveda/includes/skip-links.shtml"-->
  <div class="zen-wrapper">
    <!--#include virtual="/faculty/aceveda/customize/site-name.html"-->
    <!--#include virtual="/faculty/aceveda/includes/header.html"-->

    <!--#include virtual="/faculty/aceveda/customize/nav.html"-->

    <div class="body-wrapper">
      <main id="main-content">
        <div id="content-wrapper">
          <!-- Place content here -->


        </div> <!-- End content-wrapper -->
      </main> <!-- End main-content -->
    </div> <!-- End body-wrapper -->


    <!--#include virtual="/faculty/aceveda/customize/footer.shtml"-->
    <!--#include virtual="/faculty/aceveda/includes/footer-sub.shtml"-->
    <!--#include virtual="/faculty/aceveda/includes/scripts.html"-->
    <!-- Put Scripts here -->
  </div>
</body>
</html>
